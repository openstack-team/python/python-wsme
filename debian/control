Source: python-wsme
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 openstack-pkg-tools (>= 99~),
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 lsb-release,
 python3-cherrypy3,
 python3-flask (>= 1.1.0),
 python3-flask-restful,
 python3-lxml,
 python3-mock,
 python3-netaddr,
 python3-pecan,
 python3-pytest,
 python3-simplegeneric,
 python3-sqlalchemy,
 python3-testtools,
 python3-transaction,
 python3-tz,
 python3-webob,
 python3-webtest,
Standards-Version: 4.1.0
Vcs-Browser: https://salsa.debian.org/openstack-team/python/python-wsme
Vcs-Git: https://salsa.debian.org/openstack-team/python/python-wsme.git
Homepage: https://opendev.org/x/wsme

Package: python3-wsme
Architecture: all
Depends:
 python3-jinja2,
 python3-netaddr,
 python3-simplegeneric,
 python3-simplejson,
 python3-tz,
 python3-webob,
 ${misc:Depends},
 ${python3:Depends},
 ${wheezy-added-deps},
Recommends:
 ${python3:Recommends},
Description: Web Services Made Easy: implement multi-protocol webservices - Python 3.x
 Web Service Made Easy (WSME) simplify the writing of REST web services by
 providing simple yet powerful typing which removes the need to directly
 manipulate the request and the response objects.
 .
 WSME can work standalone or on top of your favorite Python web (micro)
 framework, so you can use both your preferred way of routing your REST requests
 and most of the features of WSME that rely on the typing system like:
 .
  * Alternate protocols, including ones supporting batch-calls
  * Easy documentation through a Sphinx extension
 .
 WSME is originally a rewrite of TGWebServices with focus on extensibility,
 framework-independance and better type handling.
 .
 This package provides the Python 3.x module.
