Getting Started
===============

For now here is just a working example.
You can find it in the examples directory of the source distribution.

.. literalinclude:: ../../examples/demo/demo.py
    :language: python
